import cv2
import numpy

red = (0, 0, 255)
green = (0, 255, 0)
black = (0, 0, 0)

ErrRange = 8
StdLeft1 = 90
StdRight1 = 90
StdLeft2 = 180
StdRight2 = 180
StdLeft3 = 100
StdRight3 = 100
StdLeft4 = 180
StdRight4 = 180


def draw(StdDeg, RelDeg, x1, y1, x2, y2, img):
    Dlt = RelDeg-StdDeg
    if Dlt > ErrRange:
        cv2.line(img, (x1, y1), (x2, y2), red, 6)
    elif Dlt < -ErrRange:
        cv2.line(img, (x1, y1), (x2, y2), black, 6)
    else:
        cv2.line(img, (x1, y1), (x2, y2), green, 6)


def drawLine(degData, poseData, x, y, img):
    x5 = int(poseData[5][1]*x)
    y5 = int(poseData[5][0]*y)
    x6 = int(poseData[6][1]*x)
    y6 = int(poseData[6][0]*y)
    x7 = int(poseData[7][1]*x)
    y7 = int(poseData[7][0]*y)
    x8 = int(poseData[8][1]*x)
    y8 = int(poseData[8][0]*y)
    x9 = int(poseData[9][1]*x)
    y9 = int(poseData[9][0]*y)
    x10 = int(poseData[10][1]*x)
    y10 = int(poseData[10][0]*y)
    x11 = int(poseData[11][1]*x)
    y11 = int(poseData[11][0]*y)
    x12 = int(poseData[12][1]*x)
    y12 = int(poseData[12][0]*y)
    x13 = int(poseData[13][1]*x)
    y13 = int(poseData[13][0]*y)
    x14 = int(poseData[14][1]*x)
    y14 = int(poseData[14][0]*y)
    x15 = int(poseData[15][1]*x)
    y15 = int(poseData[15][0]*y)
    x16 = int(poseData[16][1]*x)
    y16 = int(poseData[16][0]*y)
    draw(StdLeft1,degData[0],x5,y5,x7,y7,img)
    draw(StdRight1,degData[1],x6,y6,x8,y8,img)
    draw(StdLeft2,degData[2],x7,y7,x9,y9,img)
    draw(StdRight2,degData[3],x8,y8,x10,y10,img)
    draw(StdLeft3,degData[4],x11,y11,x13,y13,img)
    draw(StdRight3,degData[5],x12,y12,x14,y14,img)
    draw(StdLeft4,degData[6],x13,y13,x15,y15,img)
    draw(StdRight4,degData[7],x14,y14,x16,y16,img)
