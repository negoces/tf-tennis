import cv2
import numpy as np
import tensorflow as tf


def tfInit(modelPath):
    """
    modelPath(string) -> 模型路径
    """
    model = tf.saved_model.load(modelPath)
    movenet = model.signatures['serving_default']
    return movenet


def tfMovenet(image, movenet):
    tf_img = cv2.resize(image, (256, 256))
    tf_img = cv2.cvtColor(tf_img, cv2.COLOR_BGR2RGB)
    tf_img = np.asarray(tf_img)
    tf_img = np.expand_dims(tf_img, axis=0)
    tf_img = tf.cast(tf_img, dtype=tf.int32)
    return movenet(tf_img)['output_0'][0][0]
