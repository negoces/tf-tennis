# Import TF and TF Hub libraries.
import tensorflow as tf
import cv2
import numpy as np
import ws

counter = 0

model = tf.saved_model.load("models/singleThunder")
movenet = model.signatures['serving_default']

# Threshold for
threshold = .15

# Loads video source (0 is for main webcam)
video_source = 0
cap = cv2.VideoCapture(video_source)

# Checks errors while opening the Video Capture
if not cap.isOpened():
    print('Error loading video')
    quit()


success, img = cap.read()

if not success:
    print('Error reding frame')
    quit()

y, x, _ = img.shape
while success:
    # A frame of video or an image, represented as an int32 tensor of shape: 256x256x3. Channels order: RGB with values in [0, 255].
    tf_img = cv2.resize(img, (256, 256))
    tf_img = cv2.cvtColor(tf_img, cv2.COLOR_BGR2RGB)
    tf_img = np.asarray(tf_img)
    tf_img = np.expand_dims(tf_img, axis=0)

    # Resize and pad the image to keep the aspect ratio and fit the expected size.
    image = tf.cast(tf_img, dtype=tf.int32)

    # Run model inference.
    outputs = movenet(image)
    # Output is a [1, 1, 17, 3] tensor.
    keypoints = outputs['output_0']
    pose = keypoints[0][0]

    img = cv2.flip(img, 1)
    a1 = a2 = a3 = a4 = a5 = a6 = a7 = a8 = 0
    if counter < 16:
        green = 0
        red = 255
        # cv.putText(img, text, org, fontFace, fontScale, fontcolor, thickness, lineType, bottomLeftOrigin)
        cv2.putText(img, 'Please stay away from the screen', (20, 30),
                    cv2.FONT_HERSHEY_COMPLEX, 1, (1, 1, 255), 1, 4)
    else:
        green = 255
        red = 0
        # right shoulder
        a1 = ws.shoulder_or_hip(img, pose[8], pose[6], pose[12], 80, 110,
                                'Please put your rihgt hand up', 'Please put your rihgt hand down', 350, 10, a1)

        # left shoulder
        a2 = ws.shoulder_or_hip(img, pose[7], pose[5], pose[11], 80, 110,
                                'Please put your left hand up', 'Please put your left hand down', 0, 10, a2)

        # right arm
        a3 = ws.arm_or_leg(img, pose[6], pose[8], pose[10],
                           160, 'Please straighten your right arm', 350, 20, a3)

        # lfet arm
        a4 = ws.arm_or_leg(img, pose[5], pose[7], pose[9],
                           160, 'Please straighten your left arm', 0, 20, a4)

        # right hip
        a5 = ws.shoulder_or_hip(img, pose[12], pose[11], pose[13], 95, 105,
                                'Please open your right hip', 'Please close your right hip', 350, 400, a5)

        # left hip
        a6 = ws.shoulder_or_hip(img, pose[11], pose[12], pose[14], 95, 105,
                                'Please open your left hip', 'Please close your left hip', 0, 400, a6)

        # right leg
        a7 = ws.arm_or_leg(img, pose[12], pose[14], pose[16],
                           160, 'Please straighten your right leg', 350, 430, a7)

        # left leg
        a8 = ws.arm_or_leg(img, pose[11], pose[13], pose[15],
                           160, 'Please straighten your left leg', 0, 430, a8)

        sum = a1+a2+a3+a4+a5+a6+a7+a8
        if sum == 0:
            # cv.putText(img, text, org, fontFace, fontScale, fontcolor, thickness, lineType, bottomLeftOrigin)
            cv2.putText(img, 'Correct', (250, 50),
                        cv2.FONT_HERSHEY_COMPLEX, 1.5, (0, 255, 0), 2, 4)

    counter = 0

    # iterate through keypoints
    for k in keypoints[0, 0, :, :]:
        # Converts to numpy array
        k = k.numpy()

        # Checks confidence for keypoint
        if k[2] > threshold:
            # The first two channels of the last dimension represents the yx coordinates (normalized to image frame, i.e. range in [0.0, 1.0]) of the 17 keypoints
            yc = int(k[0] * y)
            xc = int((1-k[1]) * x)

            # Draws a circle on the image for each keypoint
            img = cv2.circle(img, (xc, yc), 2, (0, green, red), 5)
            counter = counter+1
    # Shows image

    cv2.imshow('Movenet', img)

    # Waits for the next frame, checks if q was pressed to quit
    if cv2.waitKey(1) == ord("q"):
        break

    # Reads next frame
    success, img = cap.read()

cap.release()
