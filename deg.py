import numpy


def calc_degABC(a, b, c):
    degBA = numpy.degrees(numpy.arctan2(a[0]-b[0], a[1]-b[1]))
    degBC = numpy.degrees(numpy.arctan2(c[0]-b[0], c[1]-b[1]))
    if degBA >= degBC:
        return degBA-degBC
    else:
        return degBA-degBC+360


def calc_degBody(pose):
    l1 = calc_degABC(pose[11], pose[5], pose[7])
    l2 = calc_degABC(pose[5], pose[7], pose[9])
    l3 = calc_degABC(pose[12], pose[11], pose[13])
    l4 = calc_degABC(pose[11], pose[13], pose[15])
    r1 = calc_degABC(pose[8], pose[6], pose[12])
    r2 = calc_degABC(pose[10], pose[8], pose[6])
    r3 = calc_degABC(pose[14], pose[12], pose[11])
    r4 = calc_degABC(pose[16], pose[14], pose[12])
    return [l1, r1, l2, r2, l3, r3, l4, r4]


def pointsCount(pose, c):
    counter = 0
    for point in pose:
        if point[2] >= c:
            counter += 1
    return counter
