import cv2
import numpy as np
import tensorflow as tf


def tfliteInit(modelPath):
    """
    modelPath(string) -> 模型路径
    """
    interpreter = tf.lite.Interpreter(model_path=modelPath)
    interpreter.allocate_tensors()
    return interpreter


def tfliteMovenet(image, interpreter):
    tf_img = cv2.resize(image, (256, 256))
    tf_img = cv2.cvtColor(tf_img, cv2.COLOR_BGR2RGB)
    tf_img = np.asarray(tf_img)
    tf_img = np.expand_dims(tf_img, axis=0)

    input_image = tf.cast(tf_img, dtype=tf.float32)
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()
    interpreter.set_tensor(input_details[0]['index'], input_image.numpy())
    interpreter.invoke()
    keypoints = interpreter.get_tensor(output_details[0]['index'])[0][0]
    return keypoints
