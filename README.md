# tf-tennis

## Usage

### 设置镜像

(可选，在虚拟环境外执行)：

```bash
pip config set global.index-url https://mirrors.bfsu.edu.cn/pypi/web/simple
# linux 建议再使用 sudo 执行一遍
sudo pip config set global.index-url https://mirrors.bfsu.edu.cn/pypi/web/simple
```

### 创建venv虚拟环境

建议使用虚拟环境，当然也可以直接使用宿主机环境，只不过可能出现兼容性问题

在当前目录下执行

```bash
# 创建 venv 虚拟环境：
python -m venv ./venv
# 激活 venv (Linux)：
source venv/bin/activate
# 激活 venv (Windows)：
./venv/bin/activate.ps1
# 检查 venv 路径：
echo $VIRTUAL_ENV
```

### 安装依赖

安装依赖(建议在虚拟环境执行)：

```bash
# Install dependencies
pip install -r requirements.txt
```

> 注：VS Code 会自动检测 `venv` 目录的虚拟环境。
