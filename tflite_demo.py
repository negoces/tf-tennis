import cv2
import tflitewrapper
import tfwrapper
import deg
import draw

tflite = False  # 是否使用TFLite
dump = False  # 是否输出结果
threshold = .15  # 保留可信度区间
video_source = 0  # 视频源

cap = cv2.VideoCapture(video_source)
if not cap.isOpened():
    print('Error loading video')
    quit()
success, img = cap.read()

if not success:
    print('Error reding frame')
    quit()

y, x, _ = img.shape
if tflite == True:
    interpreter = tflitewrapper.tfliteInit("models/singleThunder_32.tflite")
else:
    movenet = tfwrapper.tfInit("models/singleThunder")

if dump == True:
    degDatas = []

while success:
    if tflite == True:
        poseData = tflitewrapper.tfliteMovenet(img, interpreter)
    else:
        poseData = tfwrapper.tfMovenet(img, movenet)
    degData = deg.calc_degBody(poseData)
    if dump == True:
        degDatas.append(degData)
    if deg.pointsCount(poseData, threshold) != 17:
        red = 255
        green = 0
        blue = 0
    else:
        red = 0
        green = 255
        blue = 0
    draw.drawLine(degData, poseData, x, y, img)
    img = cv2.flip(img, 1)
    for k in poseData:
        if k[2] > threshold:
            yc = int(k[0] * y)
            xc = int((1-k[1]) * x)
            img = cv2.circle(img, (xc, yc), 2, (blue, green, red), 5)
    cv2.imshow('Movenet', img)
    if cv2.waitKey(1) == ord("q"):
        break
    success, img = cap.read()
cap.release()

if dump == True:
    file = open("output.txt", "w")
    for degData in degDatas:
        print(degData, file=file)
    file.close()
