import math
import cv2

def cal_ang(point_1, point_2, point_3):
    """
    根据三点坐标计算夹角
    :param point_1: 点A坐标
    :param point_2: 点B坐标
    :param point_3: 点C坐标
    :return: 返回任意角的夹角值，这里是返回点B的夹角
    """
    a = math.sqrt((point_2[0]-point_3[0])*(point_2[0]-point_3[0]) +
                  (point_2[1]-point_3[1])*(point_2[1] - point_3[1]))
    b = math.sqrt((point_1[0]-point_3[0])*(point_1[0]-point_3[0]) +
                  (point_1[1]-point_3[1])*(point_1[1] - point_3[1]))
    c = math.sqrt((point_1[0]-point_2[0])*(point_1[0]-point_2[0]) +
                  (point_1[1]-point_2[1])*(point_1[1]-point_2[1]))
    # ax = (a*a-b*b-c*c)/(-2*b*c)
    # cx = (c*c-a*a-b*b)/(-2*a*b)
    bx = (b*b-a*a-c*c)/(-2*a*c)
    if bx < -1:
        bx = -1.0
    elif bx > 1:
        bx = 1.0
    B = math.degrees(math.acos(bx))
    # A=math.degrees(math.acos(ax))
    # C=math.degrees(math.acos(cx))
    return B


def shoulder_or_hip(img, pose1, pose2, pose3, max, min, text1, text2, x, y, ai):
    if cal_ang(pose1, pose2, pose3) < max:
        # cv.putText(img, text, org, fontFace, fontScale, fontcolor, thickness, lineType, bottomLeftOrigin)
        cv2.putText(img, text1, (x, y), cv2.FONT_HERSHEY_COMPLEX,
                    0.5, (1, 1, 255), 1, 4)
        ai = 1
    elif cal_ang(pose1, pose2, pose3) > min:
        # cv.putText(img, text, org, fontFace, fontScale, fontcolor, thickness, lineType, bottomLeftOrigin)
        cv2.putText(img, text2, (x, y), cv2.FONT_HERSHEY_COMPLEX,
                    0.5, (1, 1, 255), 1, 4)
        ai = 1
    else:
        ai = 0
    return ai

def arm_or_leg(img, pose1, pose2, pose3, max, text, x, y, ai):
    if cal_ang(pose1, pose2, pose3) < max:
        # cv.putText(img, text, org, fontFace, fontScale, fontcolor, thickness, lineType, bottomLeftOrigin)
        cv2.putText(img, text, (x, y), cv2.FONT_HERSHEY_COMPLEX,
                    0.5, (1, 1, 255), 1, 4)
        ai = 1
    else:
        ai = 0
    return ai
